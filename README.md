# ТЗ9 Объединенная библиотека

Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине Оптимизационные задачи в машинном обучении.

## Демонстрация возможностей

Множество примеров работы библиотеки доступны к просмотру любым удобным для Вас способом. <br/>
* Онлайн Google Colab Notebook - нажмите [открыть Google Colab](https://colab.research.google.com/drive/1_X1EqETtI2BkAAZZUSx1QPV0KnCLFV0y?usp=sharing)
* Оффлайн Jupyter Notebook - нажмите [скачать tz9_merging_library.ipynb](tz9_merging_library.ipynb)

## Подготовка окружения

```
# Для визуализации необходим CUDA
!nvidia-smi
!pip install GPUtil
```

## Импорт модулей

```
# Установка библиотеки
!pip install git+https://leonidalekseev:maCRxpzWvEcohHKKTeG9@gitlab.com/optimization-tasks-in-machine-learning/tz9_merging_library.git
# Импорт всех модулей
import tz1_find_extremums, tz2_numerical_optimization, \
  tz3_multidimensional_optimization, tz4_regression, \
  tz5_constrained_optimization, tz6_classification, \
  tz7_cutting_optimisation, tz8_stochastic_optimization
```

## Документация функций

`help(tz1_find_extremums.utils)`

```
Help on module tz1_find_extremums.utils in tz1_find_extremums:

NAME
    tz1_find_extremums.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz1_find_extremums

FUNCTIONS
    find_extremums(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_bound_function: Union[str, NoneType] = None, bound_symbols: Union[dict, NoneType] = None, is_display_input: bool = True, is_display_solution: bool = True, is_display_conclusion: bool = True, is_approximate_calculations: bool = False, is_try_visualize: bool = True) -> list
        Поиск экстремума функции с заданными параметрами. 
        Ограничивающая функция и ограничения переменных устанавливаются.
        Отображение введенной и ограничивающий функции настраиваются.
        Отображение решения и вывода настраиваются.
        Приближенные вычислениями настраиваются.
        Визуализация графика настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        input_bound_function: str, optional
            Ограничивающая функция
        bound_symbols: dict, optional
            Ограничения переменных
        is_display_input: bool = True
            Отображать входную функцию или нет
        is_display_solution: bool = True
            Отображать решение или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет
        is_approximate_calculations: bool = False
            Приближенные вычисления или нет
        is_try_visualize: bool = True
            Визуализация графика функции или нет
        
        Returns
        ===========
        
        points_list: tuple
            Возвращаемый список точек
    
    run_gui(stop: Union[int, NoneType] = None, is_run_function: bool = True) -> dict
        Оболочка для запуска функции find_extremums.
        Опрос по каждому параметру функции find_extremums. 
        Выполните help(find_extremums), чтобы узнать о параметрах.
        
        Parameters
        ===========
        
        stop: int, optional
            Индекс последнего вопроса - параметра функции
        is_run_function: bool
            Запускать функцию с указынными параметрами или нет
        
        Returns
        ===========
        
        parametrs: dict
            Возвращаемый словарь ответов - параметров функции
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, points_list: Union[tuple, NoneType] = None, input_bound_function: Union[str, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
        Визуализация функции с точками. 
        Границы графика и детализация настроивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Исследуемая функция
        functions_symbols: tuple, optional
            Переменные функции
        points_list: tuple, optional
            Точки для визуализации
        input_bound_function: str, optional
            Ограничивающая функция
        is_display_input: bool
            Отображать входную функцию или нет
        is_autosize_graph: bool
            Автоматические устанавливать границы или нет
        start: int
            Начало графика
        stop: int
            Конец графика
        detail: int
            Детализация графика
        step: float, optional
            Единичный отрезок графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz1_find_extremums/utils.py
```

`help(tz2_numerical_optimization.utils)`

```
Help on module tz2_numerical_optimization.utils in tz2_numerical_optimization:

NAME
    tz2_numerical_optimization.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz2_numerical_optimization

FUNCTIONS
    numerical_optimization(method: Union[int, str], input_investigated_function: str, bound_function: Union[tuple, NoneType] = None, start_point: Union[tuple, NoneType] = None, accuracy: float = 1e-05, max_iterations: int = 500, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Численная птимизация одномерной функции с заданными параметрами. 
        
        Parameters
        ===========
        
        method: int, str
            Численные методы одномерной оптимизации
            0, 'golden section': метод золотого сечения
            1, 'parabola': метод парабол
            2, 'brent': метод Брента
            3, 'bfgs': метод БФГС
        input_investigated_function: str
            Входная строка с функцией
        bound_function: tuple, optional
            Начальная точка в методах 0, 1, 2
        start_point: tuple, optional
            Начальная точка в методе 3
        accuracy: float
            Точность во всех методах
        max_iterations: int
            Максимум итераций во всех методах
        is_display_input: bool = True
            Отображать входную функцию или нет
        is_display_solution: bool = True
            Отображать решение или нет
        is_save_solution: bool = False
            Сохранять решение или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет               
        is_try_visualize: bool = False
            Визуализация процесса в методе 1
        
        Returns
        ===========
        
        function_point: numpy.float64
            Найденая точка экстремума
        function_value: numpy.float64
            Значение функции в точке экстремума
        flag_process: int
            Флаг завершения алгоритма
            0: найдено значение с заданной точностью
            1: достигнуто максимальное количество итераций
            2: выполнено с ошибкой
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_bound_function: Union[str, NoneType] = None, parabols_functions: tuple = (), extremum_points: tuple = (), is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
        Визуализация функции с оптимизацией численном методом парабол. 
        
        Parameters
        ===========
        
        input_investigated_function: str
            Исследуемая функция
        functions_symbols: tuple, optional
            Переменные функции
        input_bound_function: str, optional
            Ограничивающая функция
        parabols_functions: tuple, optional
            Параболы оптимизации
        extremum_points: tuple, optional
            Точки экстремума
        is_display_input: bool
            Отображать входную функцию или нет
        start: int, float
            Начало графика
        stop: int, float
            Конец графика
        detail: int, float
            Детализация графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz2_numerical_optimization/utils.py
```

`help(tz3_multidimensional_optimization.utils)`

```
Help on module tz3_multidimensional_optimization.utils in tz3_multidimensional_optimization:

NAME
    tz3_multidimensional_optimization.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz3_multidimensional_optimization

FUNCTIONS
    multidimensional_optimization(method: Union[int, str], input_investigated_function: str, input_gradient_function: str = None, start_point: Union[tuple, NoneType] = None, accuracy: float = 1e-05, max_iterations: int = 500, gamma: float = 0.1, momentum: float = 0.8, crush_bias: float = 0.1, bias: Union[tuple, NoneType] = None, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Оптимизация многомерной функции с заданными параметрами. 
        
        Parameters
        ===========
        
        method: int, str
            Методы оптимизации
            0, 'constant': метод градиентного спуска с постоянным шагом
            1, 'step_crush': метод градиентного спуска с дробленим шага
            2, 'optimal': метод наискорейшего градиентного спуска
            3, 'co_newton': метод Ньютон-сопряженного градиентного спуска
            4, 'lr_momentum': метод градиентного спуска с lr и momentum
            
        input_investigated_function: str
            Входная строка с функцией
        input_gradient_function: str, optional
            Входная строка с функцией градиента
        start_point: tuple, optional
            Начальная точка в методе 3
        accuracy: float
            Точность во всех методах
        max_iterations: int
            Максимум итераций во всех методах
        gamma: float
            Коэфициент силы смещения (шага)
        momentum: float
            Коэфициент импульса для метода 4
        crush_bias: float
            Коэфициент дробления шага для метода 1
        bias: tuple, optional
            Начальные значения смещения (шага)
        is_display_input: bool = True
            Отображать входную функцию или нет
        is_display_solution: bool = True
            Отображать решение или нет
        is_save_solution: bool = False
            Сохранять решение или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет               
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        function_point: numpy.float64
            Найденая точка экстремума
        function_value: numpy.float64
            Значение функции в точке экстремума
        flag_process: int
            Флаг завершения алгоритма
            0: найдено значение с заданной точностью
            1: достигнуто максимальное количество итераций
            2: выполнено с ошибкой
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, points_list: Union[tuple, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
        Визуализация функции с точками. 
        Границы графика и детализация настроивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Исследуемая функция
        functions_symbols: tuple, optional
            Переменные функции
        points_list: tuple, optional
            Точки для визуализации
        is_display_input: bool
            Отображать входную функцию или нет
        is_autosize_graph: bool
            Автоматические устанавливать границы или нет
        start: int
            Начало графика
        stop: int
            Конец графика
        detail: int
            Детализация графика
        step: float, optional
            Единичный отрезок графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz3_multidimensional_optimization/utils.py
```

`help(tz4_regression.utils)`

```
Help on module tz4_regression.utils in tz4_regression:

NAME
    tz4_regression.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz4_regression

FUNCTIONS
    regression(X: list, y: list, regression_method: Union[int, str], polynomial_degree: Union[int, NoneType] = None, regularization_method: Union[int, str, NoneType] = None, L_degree: float = 100.0, is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Регрессия с заданными параметрами. 
        
        Parameters
        ===========
        
        X: list, numpy.ndarray
            Массив предикторов
        y: list, numpy.ndarray
            Массив предсказываемой переменной
        regression_method: int, str
            Методы регрессии
            0, 'linear': линейная
            1, 'polynomial': полиномиальная
            2, 'exponencial': экспоненциальная
        polynomial_degree: int, oprional
            Степень полиномиальной регрессии
        regularization_method: int, str, optional
            Методы регуляризации
            0, L1
            1, L2
        L_degree: float
            Степень L-регуляризации
        is_display_input: bool = False
            Отображать входные данные или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        function_result: str
            Получившееся функция
        coefficients: list
            Массив коэффициентов
        free_term: float
            Свободный член
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(X, y, input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
        Визуализация функции с оптимизацией численном методом парабол. 
        
        Parameters
        ===========
        
        X: list, numpy.ndarray
            Массив предикторов
        y: list, numpy.ndarray
        input_investigated_function: str
            Исследуемая функция
        functions_symbols: tuple, optional
            Переменные функции
            Массив предсказываемой переменной
        is_display_input: bool
            Отображать входную функцию или нет
        start: int, float
            Начало графика
        stop: int, float
            Конец графика
        detail: int, float
            Детализация графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz4_regression/utils.py
```

`help(tz5_constrained_optimization.utils)`

```
Help on module tz5_constrained_optimization.utils in tz5_constrained_optimization:

NAME
    tz5_constrained_optimization.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz5_constrained_optimization

FUNCTIONS
    constrained_optimization(method: Union[int, str], input_investigated_function: str, input_constraints: list, start_point: Union[tuple, NoneType] = None, accuracy: float = 0.01, max_iterations: int = 500, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Оптимизация с ограничениями с заданными параметрами. 
        
        Parameters
        ===========
        
        method: int, str
            Методы оптимизации
            0, 'newton': метод Ньютона
            1, 'log_constraints': метод логарифмических барьеров
            2, 'primal_dual': прямо-двойственный метод внутренней точки
        input_investigated_function: str
            Исследуемая функция
        input_constraints: list
            Набор ограничений типа равенств и неравенств
        start_point: tuple, optional
            Начальная точка
        accuracy: float
            Точность
        max_iterations: int
            Максимум итераций
        is_display_input: bool = True
            Отображать входную функцию или нет
        is_display_solution: bool = True
            Отображать решение или нет
        is_save_solution: bool = False
            Сохранять решение или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет               
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        function_point: numpy.float64
            Найденая точка экстремума
        function_value: numpy.float64
            Значение функции в точке экстремума
        flag_process: int
            Флаг завершения алгоритма
            0: найдено значение с заданной точностью
            1: достигнуто максимальное количество итераций
            2: выполнено с ошибкой
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_constraints: list = [], points_list: Union[tuple, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
        Визуализация функции с точками. 
        Границы графика и детализация настроивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Исследуемая функция
        input_constraints: list
            Набор ограничений типа равенств и неравенств
        functions_symbols: tuple, optional
            Переменные функции
        points_list: tuple, optional
            Точки для визуализации
        is_display_input: bool
            Отображать входную функцию или нет
        is_autosize_graph: bool
            Автоматические устанавливать границы или нет
        start: int
            Начало графика
        stop: int
            Конец графика
        detail: int
            Детализация графика
        step: float, optional
            Единичный отрезок графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz5_constrained_optimization/utils.py
```

`help(tz6_classification.utils)`

```
Help on module tz6_classification.utils in tz6_classification:

NAME
    tz6_classification.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz6_classification

FUNCTIONS
    classification(X_train: Union[list, numpy.ndarray], y_train: Union[list, numpy.ndarray], X_test: Union[list, numpy.ndarray], y_test: Union[list, numpy.ndarray], classification_method: Union[int, str], regularization_method: Union[int, str, NoneType] = None, accuracy: float = 0.0001, max_iterations: int = 1000, is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Классификация с заданными параметрами. 
        
        Parameters
        ===========
        
        X_train: list, numpy.ndarray
            Массив обучающей выборки
        y_train: list, numpy.ndarray
            Массив предсказываемой переменной
        X_test: list, numpy.ndarray
            Массив тестовой выборки
        y_test: list, numpy.ndarray
            Массив предсказываемой переменной
        classification_method: int, str
            Методы классификации
            0, 'log', 'logistic': на основе логистической регрессии
            1, 'svm', 'support_vector': на основе метода опорных векторов
        regularization_method: int, str, optional
            Методы регуляризации
            0, 'l1': L1 регуляризация
            1, 'l2': L2 регуляризация
            2, 'elasticnet': L1 + L2 регуляризация, 
                только в логистической регрессии
        accuracy: float
            Точность
        max_iterations: int
            Максимум итераций
        is_display_input: bool = False
            Отображать входные данные или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        predictions: list
            Получившееся функция
        coefficients: list
            Массив коэффициентов
        bias: float
            Смещение
        accuracy: float
            Точность
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(X: Union[list, numpy.ndarray], y: Union[list, numpy.ndarray], ground_truth: Union[list, numpy.ndarray], predictions: Union[list, numpy.ndarray], input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100) -> None
        Визуализация полученной функции с предикторами. 
        
        Parameters
        ===========
        
        X: list, numpy.ndarray
            Массив кординат по x
        y: list, numpy.ndarray
            Массив кординат по y
        ground_truth: list, numpy.ndarray
            Массив истинных предсказаний
        predictions: list, numpy.ndarray
            Массив предсказаний модели
        input_investigated_function: str
            Исследуемая функция
        functions_symbols: tuple, optional
            Переменные функции
            Массив предсказываемой переменной
        is_display_input: bool
            Отображать входную функцию или нет
        start: int, float
            Начало графика
        stop: int, float
            Конец графика
        detail: int, float
            Детализация графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz6_classification/utils.py
```

`help(tz7_cutting_optimisation.utils)`

```
Help on module tz7_cutting_optimisation.utils in tz7_cutting_optimisation:

NAME
    tz7_cutting_optimisation.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz7_cutting_optimisation

FUNCTIONS
    cutting_optimization(method: Union[int, str], input_investigated_function: str, input_constraints: list, is_display_input: bool = True, is_display_solution: bool = True, is_save_solution: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Оптимизация с ограничениями с заданными параметрами. 
        
        Parameters
        ===========
        
        method: int, str
            Методы оптимизации
            0, 'gomori': метод Гомори
            1, 'branches_and_bound': метод ветвей и границ
        input_investigated_function: str
            Исследуемая функция
        input_constraints: list
            Набор ограничений типа равенств и неравенств
        is_display_input: bool = True
            Отображать входную функцию или нет
        is_display_solution: bool = True
            Отображать решение или нет
        is_save_solution: bool = False
            Сохранять решение или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет               
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        function_point: float
            Найденая точка экстремума
        function_value: float
            Значение функции в точке экстремума
        flag_process: int
            Флаг завершения алгоритма
            0: найдено значение с заданной точностью
            1: достигнуто максимальное количество итераций
            2: выполнено с ошибкой
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    simplex(c, A, b)
    
    visualize_plotly(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, input_constraints: list = [], points_list: Union[tuple, NoneType] = None, is_display_input: bool = True, is_autosize_graph: bool = True, start: int = -1, stop: int = 1, detail: int = 100, step: Union[float, NoneType] = None) -> None
        Визуализация функции с точками. 
        Границы графика и детализация настроивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Исследуемая функция
        input_constraints: list
            Набор ограничений типа равенств и неравенств
        functions_symbols: tuple, optional
            Переменные функции
        points_list: tuple, optional
            Точки для визуализации
        is_display_input: bool
            Отображать входную функцию или нет
        is_autosize_graph: bool
            Автоматические устанавливать границы или нет
        start: int
            Начало графика
        stop: int
            Конец графика
        detail: int
            Детализация графика
        step: float, optional
            Единичный отрезок графика

FILE
    /usr/local/lib/python3.7/dist-packages/tz7_cutting_optimisation/utils.py
```

`help(tz8_stochastic_optimization.utils)`

```
Help on module tz8_stochastic_optimization.utils in tz8_stochastic_optimization:

NAME
    tz8_stochastic_optimization.utils

DESCRIPTION
    Подготовлено студентами Финансового университета при Правительстве Российской Федерации по дисциплине
    Оптимизационные задачи в машинном обучении. Для ознакомления с README модуля откройте страницу
    https://gitlab.com/optimization-tasks-in-machine-learning/tz8_stochastic_optimization

FUNCTIONS
    classification(X_train: list, y_train: list, X_test: list, y_test: list, method: Union[int, str] = 0, C_regularisation: float = 0.1, max_iterations: int = 1000, batch_size: int = 50, learning_rate: list = [0.1, 0.001], loss_function: str = 'hinge', is_display_input: bool = False, is_display_conclusion: bool = True, is_try_visualize: bool = False) -> tuple
        Классификация с заданными параметрами. 
        
        Parameters
        ===========
        
        X_train: list, numpy.ndarray
            Массив обучающей выборки
        y_train: list, numpy.ndarray
            Массив предсказываемой переменной
        X_test: list, numpy.ndarray
            Массив тестовой выборки
        y_test: list, numpy.ndarray
            Массив предсказываемой переменной
        method: int, str
            Методы классификации
            0, 'svm_sgd': методом опорных векторов с 
                использованием градиентного спуска
        C_regularisation: float
            Параметр регуляризации
        max_iterations: int = 1000
            Максимум итераций
        batch_size: int = 50
            Размер батча
        learning_rate: list = [1e-1,1e-3]
            Скорость обучения
        loss_function: str = "hinge"
            Функция ошибки
            'hinge': функция ошибки шарнира
            'sq_hinge': квадратичная функция ошибки шарнира
            'logistic': логистическая функция ошибки
        is_display_input: bool = False
            Отображать входные данные или нет
        is_display_conclusion: bool = True
            Отображать вывод или нет
        is_try_visualize: bool = False
            Визуализация процесса
        
        Returns
        ===========
        
        predictions: list
            Получившееся функция
        coefficients: list
            Массив коэффициентов
        free_term: float
            Свободный член
    
    set_function(input_investigated_function: str, functions_symbols: Union[tuple, NoneType] = None, functions_title: str = 'Введенная \\space функция:', functions_designation: str = 'F', is_display_input: bool = True, _is_system: bool = False) -> tuple
        Установка объекта sympy функции из строки.
        Количество переменных проверяется. Отображение функции настраивается.
        
        Parameters
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        functions_symbols: tuple, optional
            Переменные функции
        functions_title: str
            Заголовок для отображения
        functions_designation: str
            Обозначение для отображения
        is_display_input: bool
            Отображать входную функцию или нет
        _is_system: bool
            Вызов функции программой или нет
        
        Returns
        ===========
        
        input_investigated_function: str
            Входная строка с функцией
        investigated_function: sympy
            Исследуемая функция
        functions_symbols: tuple
            Переменные функции
    
    visualize_plotly(X: Union[list, numpy.ndarray], y: Union[list, numpy.ndarray], ground_truth: Union[list, numpy.ndarray], predictions: Union[list, numpy.ndarray], is_display_input: bool = True, start: Union[int, float] = -1, stop: Union[int, float] = 1, detail: Union[int, float] = 100, contour: Union[tuple, NoneType] = None) -> None
        Визуализация полученной функции с предикторами. 
        
        Parameters
        ===========
        
        X: list, numpy.ndarray
            Массив кординат по x
        y: list, numpy.ndarray
            Массив кординат по y
        ground_truth: list, numpy.ndarray
            Массив истинных предсказаний
        predictions: list, numpy.ndarray
            Массив предсказаний модели
        is_display_input: bool
            Отображать входную функцию или нет
        start: int, float
            Начало графика
        stop: int, float
            Конец графика
        detail: int, float
            Детализация графика
        contour: tuple, optional
            Параметры контура

FILE
    /usr/local/lib/python3.7/dist-packages/tz8_stochastic_optimization/utils.py
```

## Участники проекта

* [Быханов Никита](https://gitlab.com/BNik2001) - Менеджер проектa, Тестировщик
* [Алексеев Леонид](https://gitlab.com/LeonidAlekseev) - Программист, Тестировщик
* [Семёнова Полина](https://gitlab.com/polli_eee) - Аналитик, Тестировщик
* [Янина Марина](https://gitlab.com/marinatdd) - Аналитик, Тестировщик
* [Буркина Елизавета](https://gitlab.com/lizaburkina) - Аналитик
* [Егорин Никита](https://gitlab.com/hadesm8) - Аналитик

`Группа ПМ19-1`

## Используемые источники

* [Sympy documentation](https://www.sympy.org/en/index.html)
* [Plotly documentation](https://plotly.com/python/)
